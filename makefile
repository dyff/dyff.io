SITE_DIR = _site

MAKEFLAGS += -j auto

.PHONY: all
all: tailwindcss eleventy

.PHONY: clean
clean:
	rm -rf $(SITE_DIR)

.PHONY: distclean
distclean:
	rm -rf node_modules

.PHONY: tailwindcss
tailwindcss:
	npx tailwindcss -i src/style.css -o static/css/style.css --watch

.PHONY: eleventy
eleventy:
	npx @11ty/eleventy --serve
