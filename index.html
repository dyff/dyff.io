---
layout: base
title: Dyff
examples:
- id: create
  current: true
  title: Create assessments
  description: |-
    Create rich AI assessments using standard Python data science tools like Jupyter
    and PyArrow. Develop your assessments locally, then upload the final version to run against
    any AI system hosted on Dyff.
  code: |-
    from dyff.client import Client
    client = Client()

    dataset = client.datasets.create_arrow_dataset("/my/local/data")
    client.datasets.upload_arrow_dataset(dataset, "/my/local/data")

    jupyter_notebook = client.modules.create_package("/my/jupyter/proj")
    client.modules.upload_package(jupyter_notebook, "/my/jupyter/proj")

- id: run
  title: Run assessments
  description: |-
    Dyff orchestrates the computational resources for scalable assessment pipelines using Kubernetes.
    You can run the AI system-under-test on Dyff, too, enabling completely private assessments where
    data never leaves the platform.
  code: |-
    from dyff.client import Client
    client = Client()
    systems_to_assess = client.inferenceservices.query(
        account="public", labels={"task": "text-completion"}
    )
    for system in systems_to_assess:
        ev = client.evaluations.create(
            {"dataset": dataset.id, "inferenceService": system.id, ...}
        )
        sc = client.safetycases.create(
            {"method": "my-ipynb", "inputs": {"completions": ev.id}, ...}
        )

- id: publish
  title: Publish results
  description: |-
    Easily publish the HTML-rendered output of your assessment notebooks hosted on the Dyff Web app.
    Assessment code and input data always stays private.

  code: |-
    from dyff.client import Client
    client = Client()

    safetycase = client.safetycases.query(
        method="my-ipynb", inferenceService="system-under-test"
    )[0]

    client.safetycases.publish(safetycase.id, "public")

    import webbrowser
    webbrowser.open(f"https://app.dyff.io/reports/{safetycase.id}")

- id: deploy
  title: Deploy and extend
  description: |-
    Deploy Dyff into any Kubernetes cluster. The Dyff code base is free
    and open source, with a modular architecture that can be extended to
    support new services, runtimes, and frameworks.
  image:
    url: https://assets.dyff.io/components.png
    width: 600
    height: 296
    alt: "Collection of logos related to Dyff software."
---

<section class="container max-w-screen-xl mx-auto py-12 px-4">
  <!-- <div class="xl:grid xl:gap-8 xl:py-12 xl:grid-cols-12 items-center"> -->
    <!-- Main brand graphic, intro text, nav buttons -->
    <div class="flex flex-col mb-4">
      <img class="h-32 mb-4" src="{{ '/static/img/logo.svg' | url }}" />
      <h1 class="mb-6 text-center text-4xl sm:text-7xl">
        What's the <span class="font-semibold">Dyff</span>erence?
      </h1>
      <p class="mb-10 text-center text-xl">
        Prototype tools to assess the public risk inherited through the adoption of AI/digital systems.
        <!--Open science infrastructure for long-lived AI safety assessments-->
      </p>
      <div class="w-full sm:block sm:w-auto flex flex-col sm:flex-row mx-auto sm:space-x-5">
        <a
          href="https://app.dyff.io"
          class="px-3 py-3 mb-4 font-medium text-center rounded-lg bg-ul-bright-blue hover:bg-ul-dark-blue text-white"
        >
          Explore the cloud app
        </a>
        <a
          href="#get-dyff-client"
          class="px-3 py-3 mb-4 font-medium text-center rounded-lg border border-gray-300 hover:bg-gray-100"
        >
          Get the Dyff client
        </a>
        <a
          href="https://docs.dyff.io"
          class="px-3 py-3 mb-4 font-medium text-center rounded-lg border border-gray-300 hover:bg-gray-100"
        >
          Documentation
        </a>
      </div>
    <!-- </div> -->
    <!-- <div class="container mx-auto max-w-2xl xl:col-span-6">
      <img class="shadow-xl" src="{{ '/static/img/screenshot.png' | url }}" />
    </div> -->
  </div>
</section>

<section class="bg-gray-300 py-12">
  <div class="container max-w-screen-xl mx-auto">
    <h2 class="px-4 mb-6 text-center text-4xl">What you can do with Dyff</h2>
    <ul class="px-4 lg:px-8 flex flex-wrap mb-4 mx-auto max-w-screen-sm lg:mx-0 lg:max-w-none" id="default-tab" data-tabs-toggle="#default-tab-content" role="tablist"
      data-tabs-active-classes="text-ul-bright-blue" data-tabs-inactive-classes="text-gray-900 hover:text-ul-bright-blue">
        {% for example in examples -%}
        <li role="presentation" class="me-8 mb-2">
          <button
            class="text-lg" id="{{ example.id }}-tab"
            data-tabs-target="#{{ example.id }}" type="button" role="tab" aria-controls="{{ example.id }}"
            {%- if example.current %} aria-current="page"{% endif %}>{{ example.title }}</button>
        </li>
        {% endfor -%}
    </ul>
    <div id="default-tab-content">
      {% for example in examples -%}
      <div class="grid lg:grid-cols-2 gap-4 px-4 lg:px-8 mx-auto max-w-screen-sm lg:mx-0 lg:max-w-none{% if not example.current %} hidden{% endif %}"
        id="{{ example.id }}" role="tabpanel" aria-labelledby="{{ example.id }}-tab">
        <div class="prose prose-lg mb-4 lg:mb-8">
          <h3>{{ example.title }}</h3>
          {{ example.description | markdown }}
        </div>
        {% if example.code %}
        {% assign codeblock = "```python\n" | append: example.code | append: "\n```" %}
        {{ codeblock | markdown }}
        {% else if example.image %}
        <img
          src="{{ example.image.url }}"
          width="{{ example.image.width }}"
          height="{{ example.image.height }}"
          alt="{{ example.image.alt }}"
        />
        {% endif %}
      </div>
      {% endfor -%}
    </div>
  </div>
</section>

<section class="container max-w-screen-xl mx-auto py-12">
  <h2 class="mb-12 text-center px-4 text-4xl">Why Dyff?</h2>
  <div class="grid lg:grid-cols-3 gap-4 lg:divide-x">
    <div class="px-4 lg:px-8 mx-auto max-w-screen-sm prose prose-lg mb-8">
      <h3 class="text-center">Assessment integrity</h3>
      <p>
        Safety assessment results are meaningless if the system under test has been trained on the test data. Dyff protects test data and selectively exposes safety assessment results so developers can't game the test.
      </p>
    </div>
    <div class="px-4 lg:px-8 mx-auto max-w-screen-sm prose prose-lg mb-8">
      <h3 class="text-center">Assessment lifetime</h3>
      <p>
        Dyff assessments are long-lived. Dyff is uncompromising on reproducibility, stores every parameter and result from every test run, and protects test data to preserve its validity over time.
      </p>
    </div>
    <div class="px-4 lg:px-8 mx-auto max-w-screen-sm prose prose-lg mb-8">
      <h3 class="text-center">Assessor viability</h3>
      <p>
        Dyff demonstrates a path to an economically sustainable evaluation ecosystem by providing a platform where assessors can develop, publish, and eventually market their assessments.
      </p>
    </div>
  </div>
</section>

<section class="bg-ul-gray-1 pt-12 pb-16">
  <div class="container max-w-screen-sm mx-auto">
    <h2 id="get-dyff-client" class="mb-8 px-4 text-center text-4xl">Get the Dyff client</h2>
    <div class="mx-4 p-0 lg:col-span-5 shadow-lg shadow-ul-bright-blue-500/50 text-xl overflow-x-auto">
      {% highlight bash %}python3 -m pip install dyff{% endhighlight %}
    </div>
  </div>
</section>
