/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./*.{html,md}",
    "./**/*.{html,md}",
    "./node_modules/flowbite/**/*.js",
  ],
  theme: {
    extend: {
      colors: {
        ul: {
          "bright-blue": "#0A32FF",
          "dark-blue": "#000095",
          "bright-green": "#00A451",
          "dark-green": "#123A28",
          "bright-red": "#CA0123",
          "dark-red": "#5B0428",
          "midnight-blue": "#122C49",
          "steel-blue": "#00689C",
          "gray-1": "#D8D9DA",
          "gray-2": "#BCBEC0",
          "gray-3": "#939698",
          "gray-4": "#58595B",
        },
      },
      fontFamily: {
        sans: [
          "Arial",
          "ui-sans-serif",
          "system-ui",
          "sans-serif",
          "Apple Color Emoji",
          "Segoe UI Emoji",
          "Segoe UI Symbol",
          "Noto Color Emoji",
        ],
      },
    },
  },
  plugins: [require("@tailwindcss/typography"), require("flowbite/plugin")],
};
