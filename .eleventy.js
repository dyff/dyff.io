const markdownIt = require("markdown-it");
const markdownItAnchor = require("markdown-it-anchor");
const pluginTOC = require("eleventy-plugin-nesting-toc");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

const markdown = markdownIt({
  html: true,
  linkify: true,
  typographer: true,
}).use(markdownItAnchor, {});

module.exports = function (eleventyConfig) {
  eleventyConfig.setUseGitIgnore(false);
  eleventyConfig.addPassthroughCopy({
    static: "static",
    "node_modules/flowbite/dist/flowbite.js": "static/js/flowbite.js",
  });
  eleventyConfig.addPassthroughCopy("_redirects");

  eleventyConfig.setLibrary("md", markdown);

  eleventyConfig.addPlugin(pluginTOC);
  eleventyConfig.addPlugin(syntaxHighlight);

  eleventyConfig.addFilter("markdown", function (value) {
    return markdown.render(value);
  });
};
